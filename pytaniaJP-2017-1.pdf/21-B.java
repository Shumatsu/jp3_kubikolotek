import javax.swing.*;
import java.awt.*;

class B {
    String a = "B";
}


class RunMe extends JFrame {
    public static JPanel contentPane = new JPanel();

    public RunMe() {
        B[] ta = {new B(), new B()};
        JList list = new JList(ta);
        setTitle("B");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(275, 260);
        setResizable(false);
        setLocationRelativeTo(null);
        contentPane.add(list, BorderLayout.WEST);
        setContentPane(contentPane);
        setVisible(true);
    }


    public static void main(String[] args) {

new RunMe();
    }
}